package br.com.tsinova.nobreakts.config;

import java.util.List;

public class Nobreakts {
    
    private Output output;
    private Beat beat;
    private List<Nobreak> nobreaks;

    public Output getOutput() {
        return output;
    }

    public void setOutput(Output output) {
        this.output = output;
    }

    public Beat getBeat() {
        return beat;
    }

    public void setBeat(Beat beat) {
        this.beat = beat;
    }

    public List<Nobreak> getNobreaks() {
        return nobreaks;
    }

    public void setNobreaks(List<Nobreak> nobreaks) {
        this.nobreaks = nobreaks;
    }   
    
}
