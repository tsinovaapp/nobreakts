package br.com.tsinova.nobreakts.config;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

public class Data {
    
    private int interval;
    private String type;
    private String action;
    private String idDocActionUpdateLast;
    private List<String> global;
    private List<Oid> oids; 
    private JsonNode appendFields;
    private Boolean sendData;

    public Data(int interval, List<Oid> oids) {
        this.interval = interval;
        this.oids = oids;
    }

    public Data() {
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public List<Oid> getOids() {
        return oids;
    }

    public void setOids(List<Oid> oids) {
        this.oids = oids;
    }    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdDocActionUpdateLast() {
        return idDocActionUpdateLast;
    }

    public void setIdDocActionUpdateLast(String idDocActionUpdateLast) {
        this.idDocActionUpdateLast = idDocActionUpdateLast;
    }    

    public List<String> getGlobal() {
        return global;
    }

    public void setGlobal(List<String> global) {
        this.global = global;
    }

    public JsonNode getAppendFields() {
        return appendFields;
    }

    public void setAppendFields(JsonNode appendFields) {
        this.appendFields = appendFields;
    }    

    public Boolean getSendData() {
        return sendData;
    }

    public void setSendData(Boolean sendData) {
        this.sendData = sendData;
    }
    
}
