package br.com.tsinova.nobreakts.config;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;

public class Nobreak {
    
    private List<Data> data;  
    private String host;
    private int port;
    private String version;
    private String community;
    private String user;
    private String password;
    private JsonNode appendFields;

    public Nobreak() {
    }    

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }    

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JsonNode getAppendFields() {
        return appendFields;
    }

    public void setAppendFields(JsonNode appendFields) {
        this.appendFields = appendFields;
    }
    
}