package br.com.tsinova.nobreakts;

import br.com.tsinova.nobreakts.config.Beat;
import br.com.tsinova.nobreakts.config.Nobreak;
import br.com.tsinova.nobreakts.config.Oid;
import br.com.tsinova.nobreakts.config.Output;
import br.com.tsinova.nobreakts.config.SnmpTraps;
import br.com.tsinova.nobreakts.utils.SnmpUtils;
import br.com.tsinova.nobreakts.utils.Util;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.snmp4j.CommandResponder;
import org.snmp4j.CommandResponderEvent;
import org.snmp4j.CommunityTarget;
import org.snmp4j.MessageDispatcher;
import org.snmp4j.MessageDispatcherImpl;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.Snmp;
import org.snmp4j.mp.MPv1;
import org.snmp4j.mp.MPv2c;
import org.snmp4j.security.Priv3DES;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TcpAddress;
import org.snmp4j.smi.TransportIpAddress;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.AbstractTransportMapping;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.MultiThreadedMessageDispatcher;
import org.snmp4j.util.ThreadPool;

public class TrapReceiver extends Thread implements CommandResponder {

    private final List<Nobreak> nobreaks;
    private final Output output;
    private final Beat beat;
    private final SnmpTraps snmpTraps;

    public TrapReceiver(List<Nobreak> nobreaks, Output output, Beat beat, SnmpTraps snmpTraps) {
        this.nobreaks = nobreaks;
        this.output = output;
        this.beat = beat;
        this.snmpTraps = snmpTraps;
    }

    @Override
    public void run() {

        try {

            TransportIpAddress address = (snmpTraps.getProtocol().equalsIgnoreCase("udp")
                    ? new UdpAddress(snmpTraps.getHost() + "/" + snmpTraps.getPort())
                    : new TcpAddress(snmpTraps.getHost() + "/" + snmpTraps.getPort()));

            AbstractTransportMapping transport;

            if (address instanceof TcpAddress) {
                transport = new DefaultTcpTransportMapping((TcpAddress) address);
            } else {
                transport = new DefaultUdpTransportMapping((UdpAddress) address);
            }

            ThreadPool threadPool = ThreadPool.create("DispatcherPool", 15);
            MessageDispatcher mDispathcher = new MultiThreadedMessageDispatcher(
                    threadPool, new MessageDispatcherImpl());

            mDispathcher.addMessageProcessingModel(new MPv1());
            mDispathcher.addMessageProcessingModel(new MPv2c());

            SecurityProtocols.getInstance().addDefaultProtocols();
            SecurityProtocols.getInstance().addPrivacyProtocol(new Priv3DES());

            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(snmpTraps.getCommunity()));

            Snmp snmp = new Snmp(mDispathcher, transport);
            snmp.addCommandResponder(this);

            transport.listen();

            Logger.getLogger(Service.class.getName())
                    .log(Level.INFO, "SNMP Trap Receiving iniciado em {0}", address);

        } catch (Exception ex) {
                Logger.getLogger(Service.class.getName())
                        .log(Level.WARNING, "Falha ao iniciar o snmp traps receiving", ex);
        }

    }

    @Override
    public synchronized void processPdu(CommandResponderEvent crEvent) {

        PDU pdu = crEvent.getPDU();
        PDUv1 pduV1 = (PDUv1) pdu;
        new SendTrap(pduV1).start();

    }

    private class SendTrap extends Thread {

        private final PDU pdu;
        private final PDUv1 pduV1;

        public SendTrap(PDU pdu) {
            this.pdu = pdu;
            this.pduV1 = (PDUv1) pdu;
        }

        public JSONArray getTraps() throws JSONException {

            JSONArray trapsArray = new JSONArray();

            Vector<? extends VariableBinding> varBinds = pdu.getVariableBindings();

            if (varBinds != null && !varBinds.isEmpty()) {

                Iterator<? extends VariableBinding> varIter = varBinds.iterator();

                while (varIter.hasNext()) {

                    VariableBinding vb = varIter.next();

                    JSONObject trapObjectJson = new JSONObject();

                    String oid = vb.getOid().toString();
                    Integer syntax = vb.getVariable().getSyntax();
                    String value = vb.getVariable().toString();
                    String syntaxString = vb.getVariable().getSyntaxString();

                    trapObjectJson.put("oid", oid);
                    trapObjectJson.put("syntax", syntax);
                    trapObjectJson.put("value", value);
                    trapObjectJson.put("syntax_string", syntaxString);

                    trapsArray.put(trapObjectJson);

                }

            }

            return trapsArray;

        }

        public JSONObject getAttributesGlobal(Nobreak nobreak) {

            List<Oid> oids = new ArrayList<>();

            nobreak.getData().forEach(data -> {
                data.getOids().forEach(oid -> {
                    if (data.getGlobal().contains(oid.getName())) {
                        if (!oids.stream().anyMatch(o -> o.getName().equalsIgnoreCase(oid.getName()))) {
                            oids.add(oid);
                        }
                    }
                });
            });

            Logger.getLogger(ReadSend.class.getName())
                    .log(Level.INFO, "Efetuando a leitura de {0} oids para o trap", oids.size());

            JSONObject attributesGlobal = new JSONObject();

            CommunityTarget target = SnmpUtils.getTarget(nobreak);

            // Leitura de oids simples
            List<Oid> listOidsSimple = SnmpUtils.getLisOidByType(oids, Oid.INTEGER, Oid.LONG, Oid.TEXT, Oid.DOUBLE);
            if (!listOidsSimple.isEmpty()) {
                try {
                    attributesGlobal = SnmpUtils.getResponseOidsSimple(target, listOidsSimple, "", null);
                } catch (Exception ex) {
                    for (Oid oid : listOidsSimple) {
                        try {
                            attributesGlobal.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            Iterator<String> it = attributesGlobal.keys();

            while (it.hasNext()) {
                String key = it.next();
                if (!oids.stream().anyMatch(o -> o.getName().equalsIgnoreCase(key))) {
                    attributesGlobal.remove(key);
                }
            }

            return attributesGlobal;

        }

        @Override
        public void run() {

            String deviceIp = pduV1.getAgentAddress().toString();
            String enterprise = pduV1.getEnterprise().toString();
            long timestampTrap = pduV1.getTimestamp();

            try {

                Nobreak nobreak = nobreaks.stream().filter(n -> n.getHost().equalsIgnoreCase(deviceIp))
                        .findFirst().orElse(null);

                if (nobreak == null) {
                    return;
                }

                JSONArray trapsArray = getTraps();
                JSONObject attributesDefault = getAttributesGlobal(nobreak);

                try {

                    JSONObject deviceJSON = new JSONObject();
                    deviceJSON.put("ip", deviceIp);
                    attributesDefault.put("device", deviceJSON);

                    attributesDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                    JSONObject metadataJSON = new JSONObject();
                    metadataJSON.put("beat", beat.getName());
                    metadataJSON.put("version", beat.getVersion());
                    attributesDefault.put("@metadata", metadataJSON);

                    JSONObject beatJSON = new JSONObject();
                    beatJSON.put("name", beat.getName());
                    beatJSON.put("version", beat.getVersion());
                    attributesDefault.put("beat", beatJSON);

                    attributesDefault.put("tags", beat.getTags());

                    attributesDefault.put("enterprise", enterprise);
                    attributesDefault.put("timestamp_trap", timestampTrap);
                    attributesDefault.put("type", "Trap");
                    
                    attributesDefault.put("client_name", beat.getClientName());
                    attributesDefault.put("client_id", beat.getClientId());
                    
                    SnmpUtils.addAppendFields(attributesDefault, nobreak, null);

                } catch (JSONException ex) {
                }

                for (int i = 0; i < trapsArray.length(); i++) {

                    JSONObject trapObjectJson = trapsArray.getJSONObject(i);

                    JSONObject json = SnmpUtils.getJson(attributesDefault, trapObjectJson);
                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.INFO, Util.prettyPrinter(json));

                    try (Socket socket = new Socket(output.getLogstash().getHost(), output.getLogstash().getPort());
                            DataOutputStream os = new DataOutputStream(
                                    new BufferedOutputStream(socket.getOutputStream()))) {
                        os.writeBytes(json.toString());
                        os.flush();
                    } catch (Exception ex) {
                        Logger.getLogger(ReadSend.class.getName())
                                .log(Level.WARNING, "Failed to send for " + deviceIp, ex);
                    }

                }

            } catch (Exception ex) {
                Logger.getLogger(ReadSend.class.getName())
                        .log(Level.WARNING, "Failed to send trap for " + deviceIp, ex);
            }

        }

    }

}
