package br.com.tsinova.nobreakts.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.json.JSONObject;

public class Util {
 
    public static final String NAME_DOC_ID = "id_doc";        
    private static final SimpleDateFormat DATE_FORMAT_US = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.000");        
        
    public static String getDatetimeForElasticsearch(Date date){
        String dateFormated = DATE_FORMAT_US.format(date)+"T"+TIME_FORMAT.format(date)+"Z";
        return dateFormated;
    }
    
    public static String getDatetimeNowForElasticsearch(){
        return Instant.now().toString();
    }         
    
    public static int getSecondsByPeriod(Date dStart, Date dEnd){        
        DateTime start = new DateTime(dStart);
        DateTime end = new DateTime(dEnd);                        
        int seconds = Seconds.secondsBetween(start, end).getSeconds();
        return seconds;        
    }        
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        
        return bd.doubleValue();
        
    }
    
    
    public static Object executeScript(String script, Object value) throws Exception{
                
        Binding binding = new Binding();
        GroovyShell shell = new GroovyShell(binding);
        
        Object valueScript;
        
        if (value instanceof String){
            
            valueScript = "\""+value.toString()+"\"";
            
        }else if (value instanceof Integer || value instanceof Double || value instanceof Float || value instanceof Long){
            
            valueScript = value;
            
        }else{
            throw new Exception("only strings, integers, doubles, floats, and longs are accepted");
        
        }                                        
        
        script = script.replace("{{value}}", valueScript.toString());
        
        Object response = shell.evaluate(script);
        
        return response;        
        
    }
    
    
    public static String getIdDoc(String idDocActionUpdateLast, JSONObject document) throws Exception{
        
        Iterator it = document.keys();
        
        while(it.hasNext()){
            
            String key = it.next().toString();
            Object value = document.get(key);
            
            if (idDocActionUpdateLast.contains("{"+key+"}")){
                idDocActionUpdateLast = idDocActionUpdateLast.replace("{"+key+"}", (value+""));
            }            
            
        }
        
        return idDocActionUpdateLast;
        
    }
    
    public static String prettyPrinter(String value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    mapper.readValue(value, Object.class));
        } catch (Exception ex) {
            return value;
        }
    }
    
    public static String prettyPrinter(JSONObject value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    mapper.readValue(value.toString(), Object.class));
        } catch (Exception ex) {
            return value.toString();
        }
    }
    
    public static String prettyPrinter(Object value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
                    value);
        } catch (Exception ex) {
            return value.toString();
        }
    }
    
    
}