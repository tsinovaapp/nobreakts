package br.com.tsinova.nobreakts.utils;

import java.util.ArrayList;
import java.util.List;

public class StatesNobreak {

    private static final List<String> LIST;

    static {

        LIST = new ArrayList<>();

        LIST.add("Condição anormal presente");
        LIST.add("Na bateria");
        LIST.add("Bateria fraca");
        LIST.add("On-line");

        LIST.add("Substitua a bateria");
        LIST.add("Comunicação serial estabelecida");
        LIST.add("AVR Boost Ativo");
        LIST.add("AVR Trim Ativo");

        LIST.add("Sobrecarga");
        LIST.add("Calibração de tempo de execução");
        LIST.add("Baterias descarregadas");
        LIST.add("Bypass manual");

        LIST.add("Software Bypass");
        LIST.add("Em Bypass devido a falha interna");
        LIST.add("Em Bypass devido a falha de fornecimento");
        LIST.add("Em Bypass devido a falha do ventilador");

        LIST.add("Sleeping on a Timer");
        LIST.add("Dormindo até que a energia da rede elétrica retorne");
        LIST.add("Ligado");
        LIST.add("Reinicializando");

        LIST.add("Perda de comunicação da bateria");
        LIST.add("Desligamento normal iniciado");
        LIST.add("Smart Boost ou Smart Trim Fault");
        LIST.add("Tensão de saída ruim");

        LIST.add("Falha do carregador de bateria");
        LIST.add("Temperatura alta da bateria");
        LIST.add("Aviso de temperatura da bateria");
        LIST.add("Temperatura crítica da bateria");

        LIST.add("Autoteste em andamento");
        LIST.add("Bateria fraca / Na bateria");
        LIST.add("Desligamento normal emitido pelo dispositivo upstream");
        LIST.add("Desligamento normal emitido por dispositivo downstream");

        LIST.add("Sem baterias conectadas");
        LIST.add("Comando sincronizado em andamento");
        LIST.add("Comando de hibernação sincronizado em andamento");
        LIST.add("Comando de reinicialização sincronizado em andamento");

        LIST.add("Desequilíbrio DC do inversor");
        LIST.add("Falha do relé de transferência");
        LIST.add("Desligamento ou incapaz de transferir");
        LIST.add("Desligamento de bateria fraca");

        LIST.add("Falha do ventilador da unidade eletrônica");
        LIST.add("Falha do relé principal");
        LIST.add("Falha do relé de bypass");
        LIST.add("Bypass temporário");

        LIST.add("Alta temperatura interna");
        LIST.add("Falha do sensor de temperatura da bateria");
        LIST.add("Entrada fora do intervalo para desvio");
        LIST.add("Sobretensão do barramento CC");

        LIST.add("Falha de PFC");
        LIST.add("Falha crítica de hardware");
        LIST.add("Modo Verde / Modo ECO");
        LIST.add("Hot Standby");

        LIST.add("Desligamento de emergência (EPO) ativado");
        LIST.add("Violação de alarme de carga");
        LIST.add("Falha de fase de desvio");
        LIST.add("Falha de comunicação interna do UPS");

        LIST.add("Modo de reforço de eficiência");
        LIST.add("Desligado");
        LIST.add("Espera");
        LIST.add("Alarme Menor ou Ambiental");        
        
    }
    
    public static String getStateByPos(int pos) {
        
        if (pos < LIST.size()){
            return LIST.get(pos);
        }        
        
        return null;
        
    }

}
