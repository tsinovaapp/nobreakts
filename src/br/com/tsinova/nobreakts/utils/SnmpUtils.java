package br.com.tsinova.nobreakts.utils;

import br.com.tsinova.nobreakts.config.Data;
import br.com.tsinova.nobreakts.config.Nobreak;
import br.com.tsinova.nobreakts.config.Oid;
import com.fasterxml.jackson.databind.JsonNode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

public class SnmpUtils {

    public static List<Oid> getLisOidByType(List<Oid> list, int... types) {
        List<Oid> newList = new ArrayList<>();
        for (Oid oid : list) {
            if (!oid.isEnabled()) {
                continue;
            }
            for (int i = 0; i < types.length; i++) {
                if (oid.getType() == types[i]) {
                    newList.add(oid);
                    break;
                }
            }
        }
        return newList;
    }

    public static CommunityTarget getTarget(Nobreak nobreak) {
        CommunityTarget target = new CommunityTarget();
        if (nobreak.getVersion().equals("2c")) {
            target.setCommunity(new OctetString(nobreak.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + nobreak.getHost() + "/" + nobreak.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version2c);
        } else if (nobreak.getVersion().equals("1")) {
            target.setCommunity(new OctetString(nobreak.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + nobreak.getHost() + "/" + nobreak.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version1);
        }
        return target;
    }

    public static PDU getPDU(List<Oid> listOids, String add) {
        PDU pdu = new PDU();
        pdu.setType(PDU.GET);
        listOids.forEach((oid) -> {
            pdu.add(new VariableBinding(new OID("." + oid.getOid() + add)));
        });
        return pdu;
    }

    public static Object getValueOID(int type, VariableBinding variableBinding) throws Exception {
        switch (type) {
            case Oid.INTEGER:
                // inteiro
                return variableBinding.getVariable().toInt();
            case Oid.TEXT:
                // texto
                return variableBinding.getVariable().toString();
            case Oid.LONG:
                // Long
                return variableBinding.getVariable().toLong();
            case Oid.DOUBLE:
                // Double
                return Double.parseDouble(variableBinding.getVariable().toString());
            default:
                break;
        }
        throw new Exception("Invalid variable type");
    }

    public static JSONObject getResponseOidsSimplePort(CommunityTarget target, Snmp snmp, List<Oid> listOidsPort, String add, Integer port) throws Exception {

        List<Oid> listOidsSimple = getLisOidByType(listOidsPort, Oid.INTEGER,
                Oid.TEXT, Oid.LONG, Oid.DOUBLE, Oid.COUNTER_REGISTER, Oid.TRAFFIC_INBOUND, Oid.TRAFFIC_OUTBOUND);

        ResponseEvent responseEvent = snmp.send(getPDU(listOidsSimple, add), target);
        PDU response = responseEvent.getResponse();

        JSONObject json = new JSONObject();

        for (Oid oid : listOidsSimple) {
            boolean flag = false;

            for (int i = 0; i < response.size(); i++) {

                VariableBinding variableBinding = response.get(i);

                if (variableBinding.getOid().toString().equalsIgnoreCase(oid.getOid() + add)) {

                    if (variableBinding.isException() || variableBinding.getVariable().isException()
                            || !variableBinding.getOid().isValid() || variableBinding.getOid().isException()) {
                        json.put(oid.getName(), oid.getValueDefault());

                    } else if (oid.isUseDefaultIfExists()) {
                        json.put(oid.getName(), oid.getDefaultIfExists());

                    } else {

                        if (oid.isUseJoinOid()) {
                            String join_id = oid.getJoinOid() + "." + variableBinding.getVariable().toString();
                            Oid newOid = new Oid();
                            newOid.setName(oid.getName());
                            newOid.setOid(join_id);
                            newOid.setType(2);
                            newOid.setValueDefault("");
                            JSONObject jsonJsonId = getResponseOidsSimple(target, Arrays.asList(newOid), "", 0);
                            json.put(oid.getName(), jsonJsonId.get(oid.getName()));

                        } else {

                            Object value;

                            value = getValueOID(oid.getType(), variableBinding);

                            if (oid.getScript() != null && !oid.getScript().isEmpty()) {
                                value = Util.executeScript(oid.getScript(), value);

                            }

                            json.put(oid.getName(), value);

                        }

                    }

                    flag = true;
                    break;

                }

            }

            if (!flag) {
                json.put(oid.getName(), oid.getValueDefault());
            }

        }

        return json;

    }

    public static JSONObject getResponseOidsSimple(CommunityTarget target,
            List<Oid> listOids, String add, Integer port) throws Exception {

        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        // leitura de variavel simples da porta
        JSONObject jsonResponseOidsSimple = getResponseOidsSimplePort(target, snmp, listOids, add, port);

        snmp.close();
        transport.close();

        return jsonResponseOidsSimple;

    }

    public static int getCounterByOid(CommunityTarget target, Snmp snmp, Oid oid) {
        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));
        int counter = 0;
        for (TreeEvent event : events) {
            if (event == null) {
                continue;
            }
            if (event.isError()) {
                continue;
            }
            VariableBinding[] varBindings = event.getVariableBindings();
            for (VariableBinding varBinding : varBindings) {
                if (varBinding == null) {
                    continue;
                }
                counter++;
            }
        }
        return counter;

    }

    public static JSONObject getResponseOidsCounter(CommunityTarget target, List<Oid> oids) throws Exception {
        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        JSONObject json = new JSONObject();

        for (Oid oid : oids) {
            int counter = getCounterByOid(target, snmp, oid);
            json.put(oid.getName(), counter);
        }

        snmp.close();
        transport.close();

        return json;
    }

    public static JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    public static JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues,
            JSONObject documentJson, List<String> global) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        for (String field : global) {
            if (documentJson.has(field)) {
                json.put(field, documentJson.get(field));
            }
        }

        return json;
    }

    public static void addAppendFields(JSONObject obj, Nobreak nobreak, Data data) {

        Iterator<Map.Entry<String, JsonNode>> it;

        if (nobreak != null && nobreak.getAppendFields() != null) {
            
            it = nobreak.getAppendFields().fields();

            while (it.hasNext()) {
                Map.Entry<String, JsonNode> node = it.next();
                try {
                    if (null == node.getValue().getNodeType()) {
                        obj.put(node.getKey(), node.getValue());
                    } else {
                        switch (node.getValue().getNodeType()) {
                            case OBJECT:
                                obj.put(node.getKey(), new JSONObject(node.getValue().toString()));
                                break;
                            case ARRAY:
                                obj.put(node.getKey(), new JSONArray(node.getValue().toString()));
                                break;
                            case BOOLEAN:
                                obj.put(node.getKey(), node.getValue().asBoolean());
                                break;
                            case NULL:
                                obj.put(node.getKey(), JSONObject.NULL);
                                break;
                            case NUMBER:
                                obj.put(node.getKey(), node.getValue().asDouble());
                                break;
                            case STRING:
                                obj.put(node.getKey(), node.getValue().asText());
                                break;
                            default:
                                obj.put(node.getKey(), node.getValue());
                                break;
                        }
                    }
                } catch (JSONException ex) {
                }
            }

        }

        if (data != null && data.getAppendFields() != null) {

            it = data.getAppendFields().fields();

            while (it.hasNext()) {
                Map.Entry<String, JsonNode> node = it.next();
                try {
                    if (null == node.getValue().getNodeType()) {
                        obj.put(node.getKey(), node.getValue());
                    } else {
                        switch (node.getValue().getNodeType()) {
                            case OBJECT:
                                obj.put(node.getKey(), new JSONObject(node.getValue().toString()));
                                break;
                            case ARRAY:
                                obj.put(node.getKey(), new JSONArray(node.getValue().toString()));
                                break;
                            case BOOLEAN:
                                obj.put(node.getKey(), node.getValue().asBoolean());
                                break;
                            case NULL:
                                obj.put(node.getKey(), JSONObject.NULL);
                                break;
                            case NUMBER:
                                obj.put(node.getKey(), node.getValue().asDouble());
                                break;
                            case STRING:
                                obj.put(node.getKey(), node.getValue().asText());
                                break;
                            default:
                                obj.put(node.getKey(), node.getValue());
                                break;
                        }
                    }
                } catch (JSONException ex) {
                }
            }

        }

    }

}
