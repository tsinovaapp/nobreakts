package br.com.tsinova.nobreakts;

import br.com.tsinova.nobreakts.config.Data;
import br.com.tsinova.nobreakts.utils.Params;
import br.com.tsinova.nobreakts.config.Nobreak;
import br.com.tsinova.nobreakts.config.Nobreakts;
import br.com.tsinova.nobreakts.utils.Util;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Service extends Thread {

    private final List<ReadSend> listReadSends;
    private Nobreakts nobreakts;
    private TrapReceiver trapReceiver;

    public Service() {
        listReadSends = new ArrayList<>();
    }

    @Override
    public void run() {

        Logger.getLogger(Service.class.getName())
                .log(Level.INFO, "Reading parameters ...");

        try (InputStream inputStream = new FileInputStream("nobreakts.json")) {

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            nobreakts = objectMapper.readValue(inputStream, Nobreakts.class);
            
            Logger.getLogger(Service.class.getName())
                    .log(Level.INFO, Util.prettyPrinter(nobreakts));

            Logger.getLogger(Service.class.getName())
                    .log(Level.INFO, "Successful reading");

        } catch (Exception ex) {
            Logger.getLogger(Service.class.getName())
                    .log(Level.WARNING, "Failed to read", ex);
            return;

        }               

        if (trapReceiver == null && nobreakts.getBeat().getSnmpTraps() != null
                && nobreakts.getBeat().getSnmpTraps().isEnable()) {
            trapReceiver = new TrapReceiver(nobreakts.getNobreaks(), nobreakts.getOutput(), nobreakts.getBeat(), nobreakts.getBeat().getSnmpTraps());
            trapReceiver.start();            
        }

        while (true) {

            Map<String, Object> plugin = null;

            try {
                plugin = Params.getParamsPlugin(nobreakts.getBeat().getElasticsearchHost(), "nobreak_params_plugin");
            } catch (Exception ex) {
            }

            Integer interval = (plugin != null
                    && plugin.containsKey("interval")) ? Integer.parseInt(plugin.get("interval").toString())
                    : null;

            if (interval != null) {

                Iterator<ReadSend> it = listReadSends.iterator();

                while (it.hasNext()) {
                    ReadSend readSend = it.next();

                    if (readSend.data.getInterval() != interval) {

                        try {
                            readSend.close();
                            readSend.interrupt();
                            readSend.join();
                            Logger.getLogger(Service.class.getName())
                                    .log(Level.INFO, "Closed read " + readSend.nobreak.getHost());
                        } catch (Exception ex) {
                        }

                        it.remove();

                    }

                }

            }

            for (Nobreak nobreak : nobreakts.getNobreaks()) {

                for (Data data : nobreak.getData()) {

                    boolean hasExists = listReadSends.stream()
                            .anyMatch(rs -> rs.nobreak.getHost().equals(nobreak.getHost())
                            && rs.data.getInterval() == data.getInterval());

                    if (!hasExists) {

                        if (interval != null) {
                            data.setInterval(interval); // define o novo intervalo                            
                        }

                        ReadSend readSend = new ReadSend(nobreak, data, nobreakts.getOutput(), nobreakts.getBeat());
                        readSend.start();
                        listReadSends.add(readSend);

                        Logger.getLogger(Service.class.getName())
                                .log(Level.INFO, "Read and send processes successfully started "
                                        + "for host " + nobreak.getHost());

                    }

                }
                
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
            }

        }

    }

    public static void main(String[] args) {
        Service service = new Service();
        service.start();
    }

}
