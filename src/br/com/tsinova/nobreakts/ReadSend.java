package br.com.tsinova.nobreakts;

import br.com.tsinova.nobreakts.config.Oid;
import br.com.tsinova.nobreakts.config.Data;
import br.com.tsinova.nobreakts.utils.Util;
import br.com.tsinova.nobreakts.config.Output;
import br.com.tsinova.nobreakts.config.Beat;
import br.com.tsinova.nobreakts.config.Nobreak;
import br.com.tsinova.nobreakts.utils.SnmpUtils;
import br.com.tsinova.nobreakts.utils.StatesNobreak;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.snmp4j.CommunityTarget;

public class ReadSend extends Thread {

    public final Nobreak nobreak;
    public final Data data;
    private final Output output;
    private boolean run;
    private final Beat beat;

    public ReadSend(Nobreak nobreak, Data data, Output output, Beat beat) {
        this.nobreak = nobreak;
        this.data = data;
        this.output = output;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    @Override
    public void run() {

        while (run) {

            try {

                Logger.getLogger(ReadSend.class.getName())
                        .log(Level.INFO, "Reading the Nobreak OIDs " + nobreak.getHost() + ", Interval " + data.getInterval() + "s");

                JSONObject document = new JSONObject();

                CommunityTarget target = SnmpUtils.getTarget(nobreak);

                // Leitura de oids simples
                List<Oid> listOidsSimple = SnmpUtils.getLisOidByType(data.getOids(), Oid.INTEGER, Oid.LONG, Oid.TEXT, Oid.DOUBLE);
                if (!listOidsSimple.isEmpty()) {
                    try {
                        document = SnmpUtils.getResponseOidsSimple(target, listOidsSimple, "", null);
                    } catch (Exception ex) {
                        for (Oid oid : listOidsSimple) {
                            try {
                                document.put(oid.getName(), oid.getValueDefault());
                            } catch (JSONException ex1) {
                            }
                        }
                    }
                }

                // Efetuando a leitura de Oids contadores
                List<Oid> listOidsCounter = SnmpUtils.getLisOidByType(data.getOids(), Oid.COUNTER_REGISTER);
                if (!listOidsCounter.isEmpty()) {
                    try {
                        JSONObject responseOidsCounter = SnmpUtils.getResponseOidsCounter(target, listOidsCounter);
                        for (Oid oid : listOidsCounter) {
                            document.put(oid.getName(), responseOidsCounter.get(oid.getName()));
                        }
                    } catch (Exception ex) {
                        for (Oid oid : listOidsCounter) {
                            try {
                                document.put(oid.getName(), oid.getValueDefault());
                            } catch (JSONException ex1) {
                            }
                        }
                    }
                }

                // status do nobreak
                Oid oid = data.getOids()
                        .stream()
                        .filter(obj -> obj.getOid()
                        .equalsIgnoreCase("1.3.6.1.4.1.318.1.1.1.11.1.1.0"))
                        .findFirst()
                        .orElse(null);
                List<JSONObject> statesJsonArray = new ArrayList<>();
                if (oid != null && document.has(oid.getName())) {
                    String value = document.getString(oid.getName()).trim();
                    if (value.length() == 64) {
                        for (int i = 0; i < value.length(); i++) {
                            String stateName = StatesNobreak.getStateByPos(i);
                            if (stateName != null) {
                                JSONObject stateJsonObject = new JSONObject();
                                stateJsonObject.put("state_name", stateName);
                                stateJsonObject.put("pos", i);
                                stateJsonObject.put("status", value.substring(i, i + 1).equalsIgnoreCase("1") ? 1 : 0);
                                statesJsonArray.add(stateJsonObject);
                            }
                        }
                    }
                }

                // Seta outros atributos, atributos que qualquer documento tem
                JSONObject attributesDefault = new JSONObject();

                try {

                    JSONObject deviceJSON = new JSONObject();
                    deviceJSON.put("ip", nobreak.getHost());
                    attributesDefault.put("device", deviceJSON);

                    attributesDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                    JSONObject metadataJSON = new JSONObject();
                    metadataJSON.put("beat", beat.getName());
                    metadataJSON.put("version", beat.getVersion());
                    attributesDefault.put("@metadata", metadataJSON);

                    JSONObject beatJSON = new JSONObject();
                    beatJSON.put("name", beat.getName());
                    beatJSON.put("version", beat.getVersion());
                    attributesDefault.put("beat", beatJSON);

                    attributesDefault.put("tags", beat.getTags());

                    attributesDefault.put("client_name", beat.getClientName());
                    attributesDefault.put("client_id", beat.getClientId());

                    SnmpUtils.addAppendFields(attributesDefault, nobreak, data);

                } catch (JSONException ex) {
                }

                if ((data.getAction() == null
                        || data.getAction().equalsIgnoreCase("append")
                        || data.getAction().equalsIgnoreCase("append_and_update_last"))
                        && document.length() > 0) {

                    // add type                    
                    if (data.getType() != null && !data.getType().isEmpty()) {
                        try {
                            document.put("type", data.getType());
                        } catch (JSONException ex) {
                        }
                    }

                    JSONObject json = SnmpUtils.getJson(attributesDefault, document);
                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.INFO, Util.prettyPrinter(json));

                    if (data.getSendData() == null || (data.getSendData() != null && data.getSendData())) {
                        try (Socket socket = new Socket(output.getLogstash().getHost(), output.getLogstash().getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(json.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send for " + nobreak.getHost(), ex);
                        }
                    }

                }

                if (data.getAction() != null && (data.getAction().equalsIgnoreCase("update_last")
                        || data.getAction().equalsIgnoreCase("append_and_update_last"))
                        && data.getIdDocActionUpdateLast() != null
                        && document.length() > 0) {

                    // add type
                    if (data.getType() != null && !data.getType().isEmpty()) {
                        try {
                            document.put("type", "Latest" + data.getType());
                        } catch (JSONException ex) {
                        }
                    }

                    document.put("id_doc", Util.getIdDoc(data.getIdDocActionUpdateLast(), document));

                    JSONObject json = SnmpUtils.getJson(attributesDefault, document);
                    Logger.getLogger(ReadSend.class.getName())
                            .log(Level.INFO, Util.prettyPrinter(json));

                    if (data.getSendData() == null || (data.getSendData() != null && data.getSendData())) {
                        try (Socket socket = new Socket(output.getLogstash().getHost(), output.getLogstash().getPort());
                                DataOutputStream os = new DataOutputStream(
                                        new BufferedOutputStream(socket.getOutputStream()))) {
                            os.writeBytes(json.toString());
                            os.flush();
                        } catch (Exception ex) {
                            Logger.getLogger(ReadSend.class.getName())
                                    .log(Level.WARNING, "Failed to send for " + nobreak.getHost(), ex);
                        }
                    }

                    // envia os estados do nobreak
                    if (!statesJsonArray.isEmpty()) {

                        for (int i = 0; i < statesJsonArray.size(); i++) {

                            JSONObject stateJsonObject = statesJsonArray.get(i);

                            json = SnmpUtils.getJson(attributesDefault, stateJsonObject, document, data.getGlobal());
                            json.put("type", "State");
                            Logger.getLogger(ReadSend.class.getName()).log(Level.INFO, Util.prettyPrinter(json));

                            if (data.getSendData() == null || (data.getSendData() != null && data.getSendData())) {
                                try (Socket socket = new Socket(output.getLogstash().getHost(), output.getLogstash().getPort());
                                        DataOutputStream os = new DataOutputStream(
                                                new BufferedOutputStream(socket.getOutputStream()))) {
                                    os.writeBytes(json.toString());
                                    os.flush();
                                } catch (Exception ex) {
                                    Logger.getLogger(ReadSend.class.getName())
                                            .log(Level.WARNING, "Failed to send for " + nobreak.getHost(), ex);
                                }
                            }

                            json.put("id_doc", Util.getIdDoc(data.getIdDocActionUpdateLast(), document) + "_pos" + i);
                            json.put("type", "LatestState");
                            Logger.getLogger(ReadSend.class.getName()).log(Level.INFO, Util.prettyPrinter(json));

                            if (data.getSendData() == null || (data.getSendData() != null && data.getSendData())) {
                                try (Socket socket = new Socket(output.getLogstash().getHost(), output.getLogstash().getPort());
                                        DataOutputStream os = new DataOutputStream(
                                                new BufferedOutputStream(socket.getOutputStream()))) {
                                    os.writeBytes(json.toString());
                                    os.flush();
                                } catch (Exception ex) {
                                    Logger.getLogger(ReadSend.class.getName())
                                            .log(Level.WARNING, "Failed to send for " + nobreak.getHost(), ex);
                                }
                            }

                        }

                    }

                }

            } catch (Exception ex) {
                Logger.getLogger(ReadSend.class.getName())
                        .log(Level.WARNING, "Failed to send for " + nobreak.getHost(), ex);
            }

            // Intervalo de espera
            try {
                Thread.sleep(data.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
